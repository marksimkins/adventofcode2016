package com.websysdev.adventofcode.day1;


import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by Mark on 02/12/2016.
 */
public class ApplicationTest {

    @Test
    public void testSplitting() {
        List<String> inputs = Application.getInputs(null);
        assertEquals(0, inputs.size());

        inputs = Application.getInputs("hello");
        assertEquals(1, inputs.size());
        assertEquals("hello", inputs.get(0));

        inputs = Application.getInputs("hello,world");
        assertEquals(2, inputs.size());
        assertEquals("hello", inputs.get(0));
        assertEquals("world", inputs.get(1));

        inputs = Application.getInputs("hello, world");
        assertEquals(2, inputs.size());
        assertEquals("hello", inputs.get(0));
        assertEquals("world", inputs.get(1));

        inputs = Application.getInputs("hello   ,     world");
        assertEquals(2, inputs.size());
        assertEquals("hello", inputs.get(0));
        assertEquals("world", inputs.get(1));
    }

    @Test
    public void testToDirectionsList() {
        List<String> inputs = Application.getInputs("L1");
        List<Application.DirectionInstruction> directions = Application.toDirections(inputs);
        assertEquals(inputs.size(), directions.size());
        assertEquals(Application.DirectionInstruction.Direction.LEFT, directions.get(0).direction);
        assertEquals(1, directions.get(0).distance);

        inputs = Application.getInputs("L1, R2");
        directions = Application.toDirections(inputs);
        assertEquals(inputs.size(), directions.size());
        assertEquals(Application.DirectionInstruction.Direction.LEFT, directions.get(0).direction);
        assertEquals(1, directions.get(0).distance);
        assertEquals(Application.DirectionInstruction.Direction.RIGHT, directions.get(1).direction);
        assertEquals(2, directions.get(1).distance);
    }

    @Test
    public void exampleOne() {
        int distance = Application.getDistance("R2, L3");
        assertEquals(5, distance);
    }

    @Test
    public void exampleTwo() {
        int distance = Application.getDistance("R2, R2, R2");
        assertEquals(2, distance);
    }

    @Test
    public void exampleThree() {
        int distance = Application.getDistance("R5, L5, R5, R3");
        assertEquals(12, distance);
    }

    @Test
    public void wildcardTest() {
        int distance = Application.getDistance("R2, R2, R2, R4");
        assertEquals(2, distance);
    }

    @Test
    public void wildcardTestTwo() {
        int distance = Application.getDistance("L3, R4, R1, L3, R2, R6");
        assertEquals(1, distance);
    }

    @Test
    public void wildcardTestThree() {
        int distance = Application.getDistance("L3, R4, R1, L3, R2, R10, R1, R1, R9");
        assertEquals(10, distance);
    }

}
