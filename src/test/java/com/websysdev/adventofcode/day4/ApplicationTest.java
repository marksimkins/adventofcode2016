package com.websysdev.adventofcode.day4;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by Mark on 06/12/2016.
 */
public class ApplicationTest {

    @Test
    public void testGetEncryptedPart() {
        assertEquals("aaaaa-bbb-z-y-x", Application.getEncryptedPart("aaaaa-bbb-z-y-x-123[abxyz]"));
        assertEquals("a-b-c-d-e-f-g-h", Application.getEncryptedPart("a-b-c-d-e-f-g-h-987[abcde]"));
        assertEquals("not-a-real-room", Application.getEncryptedPart("not-a-real-room-404[oarel]"));
        assertEquals("totally-real-room", Application.getEncryptedPart("totally-real-room-200[decoy]"));
    }

    @Test
    public void testGetSectorId() {
        assertEquals(123, Application.getSectorId("aaaaa-bbb-z-y-x-123[abxyz]"));
        assertEquals(987, Application.getSectorId("a-b-c-d-e-f-g-h-987[abcde]"));
        assertEquals(404, Application.getSectorId("not-a-real-room-404[oarel]"));
        assertEquals(200, Application.getSectorId("totally-real-room-200[decoy]"));
    }

    @Test
    public void testGetChecksum() {
        assertEquals("abxyz", Application.getChecksum("aaaaa-bbb-z-y-x-123[abxyz]"));
        assertEquals("abcde", Application.getChecksum("a-b-c-d-e-f-g-h-987[abcde]"));
        assertEquals("oarel", Application.getChecksum("not-a-real-room-404[oarel]"));
        assertEquals("decoy", Application.getChecksum("totally-real-room-200[decoy]"));
    }

    @Test
    public void examples() {
        assertTrue(Application.isRealRoom("aaaaa-bbb-z-y-x-123[abxyz]"));
        assertTrue(Application.isRealRoom("a-b-c-d-e-f-g-h-987[abcde]"));
        assertTrue(Application.isRealRoom("not-a-real-room-404[oarel]"));
        assertFalse(Application.isRealRoom("totally-real-room-200[decoy]"));
    }

    @Test
    public void examples2() {
        assertTrue(Application.isRealRoom("aaaaa-bbb-z-y-x-123[abxyz]"));
        assertTrue(Application.isRealRoom("a-b-c-d-e-f-g-h-987[abcde]"));
        assertTrue(Application.isRealRoom("not-a-real-room-404[oarel]"));
        assertFalse(Application.isRealRoom("totally-real-room-200[decoy]"));
    }

    @Test
    public void decryptRoom() {
        assertEquals("very encrypted name", Application.decryptRoom("qzmt-zixmtkozy-ivhz-343[abxyz]"));
    }

}
