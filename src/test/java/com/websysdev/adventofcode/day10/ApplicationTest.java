package com.websysdev.adventofcode.day10;

import com.websysdev.adventofcode.day10.bots.Bot;
import com.websysdev.adventofcode.day10.bots.BotInstruction;
import javafx.util.Pair;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by Mark on 13/12/2016.
 */
public class ApplicationTest {

    /*
    Example:

    value 5 goes to bot 2
    bot 2 gives low to bot 1 and high to bot 0
    value 3 goes to bot 1
    bot 1 gives low to output 1 and high to bot 0
    bot 0 gives low to output 2 and high to output 0
    value 2 goes to bot 2
     */

    /*
    Possible process:

    1. Parse file, construct hashmap of bots, a list of bots and bot rules, a list of value commands.
    2. Iterate over value commands, give all bots the corresponding chips.
    3. Iterate over bot rules, sharing chips as described.
    4. Iterate over final bots, looking for the one that has 61 and 17 chips. (NOTE: this may be has, or only has.)
     */

    @Test
    public void simpleBotTest() {
        Bot bot = new Bot(0);
        assertEquals(0, bot.getChips().size());
        bot.receiveChip(1);
        assertEquals(1, bot.getChips().size());
        bot.receiveChip(2);
        assertEquals(2, bot.getChips().size());
    }

    @Test
    public void botInstructionTest() {
        BotInstruction instruction = new BotInstruction("bot 1 gives low to bot 1 and high to bot 0");
        assertEquals(1, instruction.getBotId());
        Pair<BotInstruction.Destination, Integer> lowInstruction = instruction.getLowInstruction();
        assertEquals(BotInstruction.Destination.BOT, lowInstruction.getKey());
        assertEquals(new Integer(1), lowInstruction.getValue());
        Pair<BotInstruction.Destination, Integer> highInstruction = instruction.getHighInstruction();
        assertEquals(BotInstruction.Destination.BOT, highInstruction.getKey());
        assertEquals(new Integer(0), highInstruction.getValue());

        instruction = new BotInstruction("bot 1 gives low to bot 1 and high to output 0");
        assertEquals(1, instruction.getBotId());
        lowInstruction = instruction.getLowInstruction();
        assertEquals(BotInstruction.Destination.BOT, lowInstruction.getKey());
        assertEquals(new Integer(1), lowInstruction.getValue());
        highInstruction = instruction.getHighInstruction();
        assertEquals(BotInstruction.Destination.OUTPUT, highInstruction.getKey());
        assertEquals(new Integer(0), highInstruction.getValue());

        instruction = new BotInstruction("bot 1 gives low to output 1 and high to bot 0");
        assertEquals(1, instruction.getBotId());
        lowInstruction = instruction.getLowInstruction();
        assertEquals(BotInstruction.Destination.OUTPUT, lowInstruction.getKey());
        assertEquals(new Integer(1), lowInstruction.getValue());
        highInstruction = instruction.getHighInstruction();
        assertEquals(BotInstruction.Destination.BOT, highInstruction.getKey());
        assertEquals(new Integer(0), highInstruction.getValue());

        instruction = new BotInstruction("bot 1 gives low to output 2 and high to output 0");
        assertEquals(1, instruction.getBotId());
        lowInstruction = instruction.getLowInstruction();
        assertEquals(BotInstruction.Destination.OUTPUT, lowInstruction.getKey());
        assertEquals(new Integer(2), lowInstruction.getValue());
        highInstruction = instruction.getHighInstruction();
        assertEquals(BotInstruction.Destination.OUTPUT, highInstruction.getKey());
        assertEquals(new Integer(0), highInstruction.getValue());
    }

    @Test
    public void botProcessingTest() {
        List<String> input = new ArrayList<>();
        input.add("value 5 goes to bot 2");
        input.add("bot 2 gives low to bot 1 and high to bot 0");
        input.add("value 3 goes to bot 1");
        input.add("bot 1 gives low to output 1 and high to bot 0");
        input.add("bot 0 gives low to output 2 and high to output 0");
        input.add("value 2 goes to bot 2");
        Application app = new Application(input);
        app.runSimulation();
        /*
        In the end, output bin 0 contains a value-5 microchip, output bin 1 contains a value-2 microchip,
        and output bin 2 contains a value-3 microchip. In this configuration, bot number 2 is responsible
        for comparing value-5 microchips with value-2 microchips.
         */
        Map<Integer, Set<Integer>> outputs = app.getOutputs();
        assertTrue(outputs.get(0).contains(5));
        assertTrue(outputs.get(1).contains(2));
        assertTrue(outputs.get(2).contains(3));

        Bot bot = app.getBots().get(2);
        assertEquals(Stream.of(5, 2).collect(Collectors.toSet()), bot.getProcessedChips());
    }
}
