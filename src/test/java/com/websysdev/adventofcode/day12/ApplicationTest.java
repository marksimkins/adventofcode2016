package com.websysdev.adventofcode.day12;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by Mark on 14/12/2016.
 */
public class ApplicationTest {

    @Test
    public void copyTest() {
        Application app = new Application(new ArrayList<>());
        Integer reg = new Integer(0);
        reg = app.copy(1, reg);
        assertEquals((Integer) 1, reg);

        Integer sourceReg = new Integer(2);
        reg = app.copy(sourceReg, reg);
        assertEquals((Integer)2, reg);
    }

    @Test
    public void incrementTest() {
        Application app = new Application(new ArrayList<>());
        Integer reg = new Integer(0);
        reg = app.increment(reg);
        assertEquals((Integer) 1, reg);
    }

    @Test
    public void decrementTest() {
        Application app = new Application(new ArrayList<>());
        Integer reg = new Integer(2);
        reg = app.decrement(reg);
        assertEquals((Integer) 1, reg);
    }

    @Test
    public void jumpTest() {
        List<String> instructions = new ArrayList<>();
        instructions.add("cpy 0 a");
        instructions.add("inc a");
        instructions.add("jnz a 2");
        instructions.add("inc a");
        Application app = new Application(instructions);
        app.process();
        assertEquals((Integer) 1, app.getRegister("a"));

        instructions = new ArrayList<>();
        instructions.add("cpy 0 a");
        instructions.add("inc a");
        instructions.add("jnz a 3");
        instructions.add("inc a");
        instructions.add("inc a");
        instructions.add("inc a");
        app = new Application(instructions);
        app.process();
        assertEquals((Integer) 2, app.getRegister("a"));

        instructions = new ArrayList<>();
        instructions.add("cpy 0 a");
        instructions.add("jnz a -2");
        instructions.add("inc a");
        app = new Application(instructions);
        app.process();
        assertEquals((Integer) 1, app.getRegister("a"));

        instructions = new ArrayList<>();
        instructions.add("cpy 5 a");
        instructions.add("inc a");
        instructions.add("dec a");
        instructions.add("jnz a -1");
        instructions.add("inc a");
        app = new Application(instructions);
        app.process();
        assertEquals((Integer) 1, app.getRegister("a"));

        instructions = new ArrayList<>();
        instructions.add("cpy 0 a");
        instructions.add("inc a");
        instructions.add("jnz 1 2");
        instructions.add("inc a");
        instructions.add("inc a");
        instructions.add("inc a");
        app = new Application(instructions);
        app.process();
        assertEquals((Integer) 3, app.getRegister("a"));

        instructions = new ArrayList<>();
        instructions.add("cpy 0 a");
        instructions.add("inc a");
        instructions.add("jnz 0 2");
        instructions.add("inc a");
        instructions.add("inc a");
        instructions.add("inc a");
        app = new Application(instructions);
        app.process();
        assertEquals((Integer) 4, app.getRegister("a"));
    }

    @Test
    public void example() {
        List<String> instructions = new ArrayList<>();
        instructions.add("cpy 41 a");
        instructions.add("inc a");
        instructions.add("inc a");
        instructions.add("dec a");
        instructions.add("jnz a 2");
        instructions.add("dec a");
        Application app = new Application(instructions);
        app.process();
        assertEquals((Integer) 42, app.getRegister("a"));
    }
}
