package com.websysdev.adventofcode.day9;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by Mark on 12/12/2016.
 */
public class ApplicationTest {

    @Test
    public void noMarkers() {
        assertEquals("ADVENT", Application.decompress("ADVENT"));
    }

    @Test
    public void singleRepetitions() {
        assertEquals("ABBBBBC", Application.decompress("A(1x5)BC"));
        assertEquals("XYZXYZXYZ", Application.decompress("(3x3)XYZ"));
    }

    @Test
    public void multipleRepetitions() {
        assertEquals("ABCBCDEFEFG", Application.decompress("A(2x2)BCD(2x2)EFG"));
    }

    @Test
    public void edgeCases() {
        assertEquals("(1x3)A", Application.decompress("(6x1)(1x3)A"));
        assertEquals("X(3x3)ABC(3x3)ABCY", Application.decompress("X(8x2)(3x3)ABCY"));
    }

    @Test
    public void partTwo() {
        /*
        (3x3)XYZ still becomes XYZXYZXYZ, as the decompressed section contains no markers.
        X(8x2)(3x3)ABCY becomes XABCABCABCABCABCABCY, because the decompressed data from the (8x2) marker is then further decompressed, thus triggering the (3x3) marker twice for a total of six ABC sequences.
        (27x12)(20x12)(13x14)(7x10)(1x12)A decompresses into a string of A repeated 241920 times.
        (25x3)(3x3)ABC(2x3)XY(5x2)PQRSTX(18x9)(3x2)TWO(5x7)SEVEN becomes 445 characters long.
         */
        assertEquals(6, Application.getLengthAfterFakePart2Decompression("ADVENT"));
        assertEquals(9, Application.getLengthAfterFakePart2Decompression("(3x3)XYZ"));
        assertEquals(20, Application.getLengthAfterFakePart2Decompression("X(8x2)(3x3)ABCY"));
 /*
        assertEquals(241920, Application.getLengthAfterFakeDecompression("(27x12)(20x12)(13x14)(7x10)(1x12)A"));

        assertEquals(445, Application.getLengthAfterFakeDecompression("(25x3)(3x3)ABC(2x3)XY(5x2)PQRSTX(18x9)(3x2)TWO(5x7)SEVEN"));*/
    }
}
