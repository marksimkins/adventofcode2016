package com.websysdev.adventofcode.day8;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by Mark on 12/12/2016.
 */
public class ApplicationTest {

    /*
    rect AxB                turns on all of the pixels in a rectangle at the top-left of the screen which is A wide and B tall.
    rotate row y=A by B     shifts all of the pixels in row A (0 is the top row) right by B pixels. Pixels that would fall off the right end appear at the left end of the row.
    rotate column x=A by B  shifts all of the pixels in column A (0 is the left column) down by B pixels. Pixels that would fall off the bottom appear at the top of the column.
     */

    private Application app;

    @Before
    public void setup() {
        app = new Application();
    }


    @Test
    public void createRectangleTest() {
        app.setRectangle(6, 3);
        app.printScreen();
    }

    @Test
    public void rotateRowTest() {
        app.setRectangle(6, 3);
        app.rotateRow(1,5);
        app.printScreen();
    }

    @Test
    public void rotateColumnTest() {
        app.setRectangle(6, 3);
        app.rotateColumn(2,8);
        app.printScreen();
    }

    @Test
    public void countOnPixelsTest() {
        app.setRectangle(0, 0);
        assertEquals(0, app.countOnPixels());

        app.setRectangle(1, 1);
        assertEquals(1, app.countOnPixels());

        app.setRectangle(2, 2);
        assertEquals(4, app.countOnPixels());
        app.rotateRow(1,5);
        app.rotateColumn(2,8);
        assertEquals(4, app.countOnPixels());
    }
}
