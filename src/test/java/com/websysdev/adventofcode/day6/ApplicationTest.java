package com.websysdev.adventofcode.day6;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by Mark on 08/12/2016.
 */
public class ApplicationTest {

    @Test
    public void examplePart1() {
        List<String> input = Arrays.asList(new String[] {
                "eedadn",
                "drvtee",
                "eandsr",
                "raavrd",
                "atevrs",
                "tsrnev",
                "sdttsa",
                "rasrtv",
                "nssdts",
                "ntnada",
                "svetve",
                "tesnvt",
                "vntsnd",
                "vrdear",
                "dvrsen",
                "enarar"
        });

        assertEquals("easter", Application.findMessage(input, new Application.MaxCharFinder()));
    }

    @Test
    public void examplePart2() {
        List<String> input = Arrays.asList(new String[] {
                "eedadn",
                "drvtee",
                "eandsr",
                "raavrd",
                "atevrs",
                "tsrnev",
                "sdttsa",
                "rasrtv",
                "nssdts",
                "ntnada",
                "svetve",
                "tesnvt",
                "vntsnd",
                "vrdear",
                "dvrsen",
                "enarar"
        });

        assertEquals("advent", Application.findMessage(input, new Application.MinCharFinder()));
    }

}
