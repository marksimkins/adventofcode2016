package com.websysdev.adventofcode.day2;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by Mark on 02/12/2016.
 */
public class ApplicationTest {

    @Test
    public void example() {
        String[] inputs = new String[] {
                "ULL",
                "RRDDD",
                "LURDL",
                "UUUUD"
        };
        String keycode = Application.getKeyCode(inputs);
        assertEquals("1985", keycode);
    }

    @Test
    public void example2() {
        String[] inputs = new String[] {
                "ULL",
                "RRDDD",
                "LURDL",
                "UUUUD"
        };
        String keycode = Application.getKeyCode2(inputs);
        assertEquals("5DB3", keycode);
    }

}
