package com.websysdev.adventofcode.day3;

import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by Mark on 05/12/2016.
 */
public class ApplicationTest {


    @Test
    public void testToDimensions() {
        List<String> dimensions = Application.toDimensions("");
        assertEquals(0, dimensions.size());

        dimensions = Application.toDimensions(" ");
        assertEquals(0, dimensions.size());

        dimensions = Application.toDimensions("  ");
        assertEquals(0, dimensions.size());

        dimensions = Application.toDimensions("1");
        assertEquals(1, dimensions.size());

        dimensions = Application.toDimensions(" 1 ");
        assertEquals(1, dimensions.size());

        dimensions = Application.toDimensions(" 1  ");
        assertEquals(1, dimensions.size());

        dimensions = Application.toDimensions("1 2");
        assertEquals(2, dimensions.size());

        dimensions = Application.toDimensions(" 1 2");
        assertEquals(2, dimensions.size());

        dimensions = Application.toDimensions(" 1  2");
        assertEquals(2, dimensions.size());

        dimensions = Application.toDimensions("1 2 3");
        assertEquals(3, dimensions.size());

        dimensions = Application.toDimensions(" 1 2 3 ");
        assertEquals(3, dimensions.size());

        dimensions = Application.toDimensions(" 1  2       3   ");
        assertEquals(3, dimensions.size());
    }

    @Test
    public void testisTriangleDims() {
        assertFalse(Application.isTriangle(Application.toDimensions("5 10 25")));
        assertFalse(Application.isTriangle(Application.toDimensions("1 2  3")));
        assertTrue(Application.isTriangle(Application.toDimensions("7 5 10")));
        assertTrue(Application.isTriangle(Application.toDimensions("5 7 10")));
        assertTrue(Application.isTriangle(Application.toDimensions("5 10 7")));

        assertFalse(Application.isTriangle(Application.toDimensions("  725  312  215")));
        assertFalse(Application.isTriangle(Application.toDimensions("  380   97  857")));
        assertFalse(Application.isTriangle(Application.toDimensions("  131  888  560")));
    }

}
