package com.websysdev.adventofcode.day15;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by Mark on 16/12/2016.
 */
public class ApplicationTest {

    /*
    Disc #1 has 17 positions; at time=0, it is at position 1.
    Disc #2 has 7 positions; at time=0, it is at position 0.
    Disc #3 has 19 positions; at time=0, it is at position 2.
    Disc #4 has 5 positions; at time=0, it is at position 0.
    Disc #5 has 3 positions; at time=0, it is at position 0.
    Disc #6 has 13 positions; at time=0, it is at position 5.
     */

    /*
    Rotating disks of varying sizes, starting at time 0 in varying positions.
    Between each disk, one second elapses.
    Each second, each disk rotates one position.
    Position 0 is a gap that allows the ball to fall through.
     */

    @Test
    public void testFindZeroRoutesOneDisk() {
        Application app = new Application();
        app.addDisk(2, 0);
        assertEquals(1, app.calculateTimeToRelease());

        app = new Application();
        app.addDisk(2, 1);
        assertEquals(0, app.calculateTimeToRelease());

        app = new Application();
        app.addDisk(3, 0);
        assertEquals(2, app.calculateTimeToRelease());

        app = new Application();
        app.addDisk(3, 1);
        assertEquals(1, app.calculateTimeToRelease());

        app = new Application();
        app.addDisk(3, 2);
        assertEquals(0, app.calculateTimeToRelease());
    }

    @Test
    public void testFindZeroRoutesMultipleDisks() {
        Application app = new Application();
        app.addDisk(2, 0);
        app.addDisk(3, 0);
        assertEquals(1, app.calculateTimeToRelease());

        app = new Application();
        app.addDisk(2, 0);
        app.addDisk(3, 1);
        assertEquals(3, app.calculateTimeToRelease());
    }
}
