package com.websysdev.adventofcode.day7;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by Mark on 09/12/2016.
 */
public class ApplicationTest {

    @Test
    public void examplesPart1() {
        assertTrue(Application.supportsTls("acmtosqnsqsxkhdrd[jkapzaeusxzibvctt]uscjxyktvaphdkkdaw[tpsekpbdlqmiuoj]qcaudtzfapwuvjzxfxu[zhbsuykivuwguukvl]cqkrjlgbidtjkihjft"));
        assertTrue(Application.supportsTls("abba[mnop]qrst"));
        assertFalse(Application.supportsTls("abcd[bddb]xyyx"));
        assertFalse(Application.supportsTls("aaaa[qwer]tyui"));
        assertTrue(Application.supportsTls("ioxxoj[asdfgh]zxcvbn"));
        assertFalse(Application.supportsTls("ioxxoj[assa]zxcvbn"));
        assertFalse(Application.supportsTls("ioxxj[assa]zxxzcvbn"));
        assertFalse(Application.supportsTls("ioxxj[assd]zxxzcvbn[abbaa]ieonin"));
        assertTrue(Application.supportsTls("ioxxj[assd]zxxzcvbn[abbba]ieonin"));
        assertFalse(Application.supportsTls("aaaa[assd]bbbbbbbbb[aba]ieonin"));
        assertFalse(Application.supportsTls("aaaa[assd]bbbbbbbbb[aba]abonin"));
    }

    /*
    aba[bab]xyz supports SSL (aba outside square brackets with corresponding bab within square brackets).
xyx[xyx]xyx does not support SSL (xyx, but no corresponding yxy).
aaa[kek]eke supports SSL (eke in supernet with corresponding kek in hypernet; the aaa sequence is not related, because the interior character must be different).
zazbz[bzb]cdb supports SSL (zaz has no corresponding aza, but zbz has a corresponding bzb, even though zaz and zbz overlap).
     */
    @Test
    public void examplesPart2() {
        assertTrue(Application.supportsSsl("aba[bab]xyz"));
        assertFalse(Application.supportsSsl("xyx[xyx]xyx"));
        assertTrue(Application.supportsSsl("aaa[kek]eke"));
        assertTrue(Application.supportsSsl("zazbz[bzb]cdb"));
    }

}
