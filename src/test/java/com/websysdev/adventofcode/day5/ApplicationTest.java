package com.websysdev.adventofcode.day5;

import com.websysdev.adventofcode.shared.Md5Hexer;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Mark on 07/12/2016.
 */
public class ApplicationTest {

    @Test
    public void testHexerWithKnown() {
        Md5Hexer hexer = new Md5Hexer();

        assertFalse(hexer.getHex("abc1").startsWith("00000"));
        assertFalse(hexer.getHex("abc3231928").startsWith("00000"));
        assertTrue(hexer.getHex("abc3231929").startsWith("00000"));
        assertFalse(hexer.getHex("abc3231930").startsWith("00000"));

        assertTrue(hexer.getHex("abc5017308").startsWith("00000"));
        assertTrue(hexer.getHex("abc5278568").startsWith("00000"));
    }

    @Test
    public void testAlgorithmPart1WithKnown() {
        assertEquals("18f47a30", Application.retrievePassword("abc"));
    }

    @Test
    public void testAlgorithmPart2WithKnown() {
        assertEquals("05ace8e3", Application.retrievePasswordPart2("abc"));
    }
}
