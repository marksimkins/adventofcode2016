package com.websysdev.adventofcode.day16;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by Mark on 22/12/2016.
 */
public class ApplicationTest {

    @Test
    public void reverseBytes() {
        byte[] reversedArr = Application.reverse(Application.stringToByteArray("0"));
        assertEquals(0, reversedArr[0]);

        reversedArr = Application.reverse(Application.stringToByteArray("1"));
        assertEquals(1, reversedArr[0]);

        reversedArr = Application.reverse(Application.stringToByteArray("01"));
        assertEquals(1, reversedArr[0]);
        assertEquals(0, reversedArr[1]);

        reversedArr = Application.reverse(Application.stringToByteArray("10"));
        assertEquals(0, reversedArr[0]);
        assertEquals(1, reversedArr[1]);

        reversedArr = Application.reverse(Application.stringToByteArray("001"));
        assertEquals(1, reversedArr[0]);
        assertEquals(0, reversedArr[1]);
        assertEquals(0, reversedArr[2]);

        reversedArr = Application.reverse(Application.stringToByteArray("100"));
        assertEquals(0, reversedArr[0]);
        assertEquals(0, reversedArr[1]);
        assertEquals(1, reversedArr[2]);
    }

    @Test
    public void swapBytes() {
        byte[] swappedArr = Application.swapBytes(Application.stringToByteArray("0"));
        assertEquals(1, swappedArr[0]);

        swappedArr = Application.swapBytes(Application.stringToByteArray("1"));
        assertEquals(0, swappedArr[0]);

        swappedArr = Application.swapBytes(Application.stringToByteArray("01"));
        assertEquals(1, swappedArr[0]);
        assertEquals(0, swappedArr[1]);

        swappedArr = Application.swapBytes(Application.stringToByteArray("10"));
        assertEquals(0, swappedArr[0]);
        assertEquals(1, swappedArr[1]);

        swappedArr = Application.swapBytes(Application.stringToByteArray("001"));
        assertEquals(1, swappedArr[0]);
        assertEquals(1, swappedArr[1]);
        assertEquals(0, swappedArr[2]);

        swappedArr = Application.swapBytes(Application.stringToByteArray("100"));
        assertEquals(0, swappedArr[0]);
        assertEquals(1, swappedArr[1]);
        assertEquals(1, swappedArr[2]);

        swappedArr = Application.swapBytes(Application.stringToByteArray("101"));
        assertEquals(0, swappedArr[0]);
        assertEquals(1, swappedArr[1]);
        assertEquals(0, swappedArr[2]);

        swappedArr = Application.swapBytes(Application.stringToByteArray("111"));
        assertEquals(0, swappedArr[0]);
        assertEquals(0, swappedArr[1]);
        assertEquals(0, swappedArr[2]);

        swappedArr = Application.swapBytes(Application.stringToByteArray("000"));
        assertEquals(1, swappedArr[0]);
        assertEquals(1, swappedArr[1]);
        assertEquals(1, swappedArr[2]);
    }

    @Test
    public void growData() {
        byte[] grownArr = Application.growData(Application.stringToByteArray("1"));
        assertEquals(1, grownArr[0]);
        assertEquals(0, grownArr[1]);
        assertEquals(0, grownArr[2]);

        grownArr = Application.growData(Application.stringToByteArray("0"));
        assertEquals(0, grownArr[0]);
        assertEquals(0, grownArr[1]);
        assertEquals(1, grownArr[2]);

        grownArr = Application.growData(Application.stringToByteArray("11111"));
        assertEquals(1, grownArr[0]);
        assertEquals(1, grownArr[1]);
        assertEquals(1, grownArr[2]);
        assertEquals(1, grownArr[3]);
        assertEquals(1, grownArr[4]);
        assertEquals(0, grownArr[5]);
        assertEquals(0, grownArr[6]);
        assertEquals(0, grownArr[7]);
        assertEquals(0, grownArr[8]);
        assertEquals(0, grownArr[9]);
        assertEquals(0, grownArr[10]);

        grownArr = Application.growData(Application.stringToByteArray("111100001010"));
        assertEquals(1, grownArr[0]);
        assertEquals(1, grownArr[1]);
        assertEquals(1, grownArr[2]);
        assertEquals(1, grownArr[3]);
        assertEquals(0, grownArr[4]);
        assertEquals(0, grownArr[5]);
        assertEquals(0, grownArr[6]);
        assertEquals(0, grownArr[7]);
        assertEquals(1, grownArr[8]);
        assertEquals(0, grownArr[9]);
        assertEquals(1, grownArr[10]);
        assertEquals(0, grownArr[11]);
        assertEquals(0, grownArr[12]);
        assertEquals(1, grownArr[13]);
        assertEquals(0, grownArr[14]);
        assertEquals(1, grownArr[15]);
        assertEquals(0, grownArr[16]);
        assertEquals(1, grownArr[17]);
        assertEquals(1, grownArr[18]);
        assertEquals(1, grownArr[19]);
        assertEquals(1, grownArr[20]);
        assertEquals(0, grownArr[21]);
        assertEquals(0, grownArr[22]);
        assertEquals(0, grownArr[23]);
        assertEquals(0, grownArr[24]);
    }

    @Test
    public void exampleGrow() {
        /*
        Combining all of these steps together, suppose you want to fill a disk of length 20 using an initial state of 10000:

        Because 10000 is too short, we first use the modified dragon curve to make it longer.
        After one round, it becomes 10000011110 (11 characters), still too short.
        After two rounds, it becomes 10000011110010000111110 (23 characters), which is enough.
         */
        byte[] finalArr = Application.fillDisk(Application.stringToByteArray("10000"), 20);
        assertEquals(1, finalArr[0]);
        assertEquals(0, finalArr[1]);
        assertEquals(0, finalArr[2]);
        assertEquals(0, finalArr[3]);
        assertEquals(0, finalArr[4]);
        assertEquals(0, finalArr[5]);
        assertEquals(1, finalArr[6]);
        assertEquals(1, finalArr[7]);
        assertEquals(1, finalArr[8]);
        assertEquals(1, finalArr[9]);
        assertEquals(0, finalArr[10]);
        assertEquals(0, finalArr[11]);
        assertEquals(1, finalArr[12]);
        assertEquals(0, finalArr[13]);
        assertEquals(0, finalArr[14]);
        assertEquals(0, finalArr[15]);
        assertEquals(0, finalArr[16]);
        assertEquals(1, finalArr[17]);
        assertEquals(1, finalArr[18]);
        assertEquals(1, finalArr[19]);
        assertEquals(1, finalArr[20]);
        assertEquals(1, finalArr[21]);
        assertEquals(0, finalArr[22]);
    }

    @Test
    public void exampleChecksum() {
        String checksum = Application.calcChecksum("110010110100", 12);
        assertEquals("100", checksum);
    }

}
