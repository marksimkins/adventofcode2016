package com.websysdev.adventofcode.day14;

import org.junit.Test;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by Mark on 15/12/2016.
 */
public class ApplicationTest {

    @Test
    public void example() {
        ApplicationWorking app = new ApplicationWorking("abc");
        app.getKeys(false);
        assertEquals(22728, app.getLastIndex());
    }

    @Test
    public void examplePart2() {
        ApplicationWorking app = new ApplicationWorking("abc");
        app.getKeys(true);
        assertEquals(22551, app.getLastIndex());
    }

    @Test
    public void detectTriple() {
        Optional<String> triple = ApplicationWorking.getInternalTriple("cc38887a5");
        assertEquals("888", triple.get());
        triple = ApplicationWorking.getInternalTriple("8887a5");
        assertEquals("888", triple.get());
        triple = ApplicationWorking.getInternalTriple("cc3888");
        assertEquals("888", triple.get());
        triple = ApplicationWorking.getInternalTriple("cc3888abtysdeee");
        assertEquals("888", triple.get());
        triple = ApplicationWorking.getInternalTriple("888");
        assertEquals("888", triple.get());
        triple = ApplicationWorking.getInternalTriple("4FC7DF57777400B3EEEF5350A6EECBDF ");
        assertEquals("777", triple.get());
        triple = ApplicationWorking.getInternalTriple("cc3887a5");
        assertFalse(triple.isPresent());
        triple = ApplicationWorking.getInternalTriple("887");
        assertFalse(triple.isPresent());
    }

    @Test
    public void detectQuintuple() {
        assertTrue(ApplicationWorking.containsMatchingQuintuple("cc3888887a5", "888"));
        assertFalse(ApplicationWorking.containsMatchingQuintuple("cc3888887a5", "777"));
        assertTrue(ApplicationWorking.containsMatchingQuintuple("888887a5", "888"));
        assertTrue(ApplicationWorking.containsMatchingQuintuple("cc388888", "888"));
        assertTrue(ApplicationWorking.containsMatchingQuintuple("88888", "888"));
        assertFalse(ApplicationWorking.containsMatchingQuintuple("cc38887a5", "888"));
    }

}
