package com.websysdev.adventofcode.day9;

import com.websysdev.adventofcode.shared.FileReader;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

/**
 * Created by Mark on 12/12/2016.
 */
public class Application {

    public static void main(String[] args) throws IOException, URISyntaxException {
        if (args.length == 0) {
            System.out.println("Please provide a filename.");
            System.exit(0);
        }
        List<String> fileContents = FileReader.readFile(args[0]);
        if (fileContents.size() > 1) {
            System.out.println("Didn't expect a file with more than one line!");
            System.exit(1);
        }
        String originalFile = fileContents.get(0);
        System.out.println("Original length: " + originalFile.length());
        System.out.println("Decompressed length: " + getLengthAfterFakePart2Decompression(originalFile));
    }

    public static String decompress(String input) {
        StringBuilder sb = new StringBuilder();
        char[] chars = input.toCharArray();
        for (int i=0; i<chars.length; i++) {
            char c  = chars[i];
            if (c == '(') {
                int indexOfClose = input.substring(i).indexOf(')');
                int indexOfNextChar = i + indexOfClose;
                String instruction = input.substring(i+1, i+indexOfClose);
                String[] parts = instruction.split("x");
                int charCount = Integer.parseInt(parts[0]);
                int repetitions = Integer.parseInt(parts[1]);

                char[] charsToRepeat = new char[charCount];
                for (int j=0; j<charCount; j++) {
                    charsToRepeat[j] = chars[indexOfNextChar + 1 + j];
                }
                for (int reps=0; reps<repetitions; reps++) {
                    for (int j = 0; j < charCount; j++) {
                        sb.append(charsToRepeat[j]);
                    }
                }
                i = indexOfNextChar + charCount;
                continue;
            }
            sb.append(c);
        }
        return sb.toString();
    }

    /*
    X(8x2)(3x3)ABCY = 20 not 18

    X(8x2)(3x3)ABCY  becomes  XABCABCABCABCABCABCY  == 20

    What this is doing is:

    X(8x2)(3x3)ABCY = 1 + 16 + 1 = 18.
     */
    public static int getLengthAfterFakePart2Decompression(String input) {
        int length = 0;
        char[] chars = input.toCharArray();
        for (int i=0; i<chars.length; i++) {
            char c  = chars[i];
            if (c == '(') {
                int indexOfClose = input.substring(i).indexOf(')');
                int indexOfNextChar = i + indexOfClose;
                String instruction = input.substring(i+1, i+indexOfClose);
                String[] parts = instruction.split("x");
                int charCount = Integer.parseInt(parts[0]);
                int repetitions = Integer.parseInt(parts[1]);

                length += charCount * repetitions;
                i = indexOfNextChar + charCount;
                // FIXME: THIS IS NOT CORRECT. Coming back to this later.
                continue;
            }
            length++;
        }
        return length;
    }

}
