package com.websysdev.adventofcode.day14;

import com.websysdev.adventofcode.shared.Md5Hexer;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * NOTE:
 *
 * This does not produce the correct answer. I prefer this implementation, as it reduces unnecessary re-processing resulting
 * in significantly higher performance, but it's off by 21. Not too sure why yet.
 *
 * Created by Mark on 15/12/2016.
 */
public class ApplicationNotWorking {

    private static final Pattern TRIPLE_PATTERN = Pattern.compile("(.)\\1{2}");

    private final String salt;
    private final Md5Hexer md5Hexer = new Md5Hexer();

    // Map of triples, to a map of matching keyIndexes and md5 strings.
    private Map<String, Map<Integer, String>> potentialKeys = new HashMap<>();
    private TreeMap<Integer, String> validKeys = new TreeMap<>();

    public static void main(String[] args) {
        String salt = "qzyelonm";

        System.out.println("Generating keys from salt: " + salt);
        ApplicationNotWorking app = new ApplicationNotWorking(salt);
        app.getKeys();
        System.out.println("The 64th index is: " + app.getLastIndex()); // 15189 is too high. 15168 is correct. Why does this work, and not the other one?
    }

    ApplicationNotWorking(String salt) {
        this.salt = salt;
    }

    public void getKeys() {
        int index = 0;
        while (validKeys.size() < 64) {
            // Tidy up tripleIndexes that we have that are no longer valid
            cleanOldIndexes(index);

            String md5key = md5Hexer.getHex(salt+index).toLowerCase();
            Optional<String> triple = getInternalTriple(md5key);

            if (triple.isPresent()) {
                String tripleStr = triple.get();
                Map<Integer, String> potentialIndexes = potentialKeys.get(tripleStr);

                if (potentialIndexes == null) {
                    potentialIndexes = new TreeMap<>();
                }

                // If there's 5 characters in it, and we have an earlier index of matching triples, the earlier index is a valid key
                if (containsMatchingQuintuple(md5key, tripleStr) && potentialIndexes.size() > 0) {
                    Iterator<Map.Entry<Integer, String>> matchingKeyIt = potentialIndexes.entrySet().iterator();
                    Map.Entry<Integer, String> validKey = matchingKeyIt.next();
                    matchingKeyIt.remove();
                    validKeys.put(validKey.getKey(), validKey.getValue());
                    continue;
                }
                potentialIndexes.put(index, md5key);
                potentialKeys.put(tripleStr, potentialIndexes);
            }
            index++;
        }
    }

    private void cleanOldIndexes(int index) {
        Iterator<Map.Entry<String,Map<Integer,String>>> potentialKeysIt = potentialKeys.entrySet().iterator();
        while (potentialKeysIt.hasNext()) {
            Map<Integer,String> indexEntry = potentialKeysIt.next().getValue();
            Iterator<Map.Entry<Integer,String>> keyIt = indexEntry.entrySet().iterator();
            while (keyIt.hasNext()) {
                if (index >= keyIt.next().getKey() + 1000) {
                    keyIt.remove();
                }
            }
            if (indexEntry.isEmpty()) {
                potentialKeysIt.remove();
            }
        }
    }

    public int getLastIndex() {
        return validKeys.lastKey();
    }


    public static Optional<String> getInternalTriple(String str) {
        Matcher match = TRIPLE_PATTERN.matcher(str);
        if (match.find()) {
            char c = match.group().charAt(0);
            return Optional.of("" + c + c + c);
        }
        return Optional.empty();
    }

    public static boolean containsMatchingQuintuple(String str, String tripleToMatch) {
        char c = tripleToMatch.charAt(0);
        String quintuple = tripleToMatch + c + c;
        return str.contains(quintuple);
    }
}
