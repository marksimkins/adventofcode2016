package com.websysdev.adventofcode.day14;

import com.websysdev.adventofcode.shared.Md5Hexer;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Mark on 15/12/2016.
 */
public class ApplicationWorking {

    private static final Pattern TRIPLE_PATTERN = Pattern.compile("(.)\\1{2}");

    private final String salt;
    private final Md5Hexer md5Hexer = new Md5Hexer();

    private Map<Integer, String> generatedMd5s = new HashMap<>();

    private TreeMap<Integer, String> validKeys = new TreeMap<>();

    public static void main(String[] args) {
        String salt = "qzyelonm";

        System.out.println("Generating keys from salt: " + salt);
        ApplicationWorking app = new ApplicationWorking(salt);
        long start = System.currentTimeMillis();
        app.getKeys(true);
        System.out.println("That took " + (System.currentTimeMillis()-start) + " millis.");
        System.out.println("The 64th index is: " + app.getLastIndex()); // 15189 is too high. 15168 is correct. Why does this work, and not the other one?
    }

    ApplicationWorking(String salt) {
        this.salt = salt;
    }

    public void getKeys(boolean part2) {
        int index = 0;
        while (validKeys.size() < 64) {

            String md5key = getMd5(index, part2);
            Optional<String> triple = getInternalTriple(md5key);

            if (triple.isPresent()) {
                String tripleStr = triple.get();

                // Find the quintuple that matches this triple.
                for (int i=index+1; i<=index+1000; i++) {
                    // I'm not so fond of this implementation, as it re-processes md5's multiple times.
                    // This works, however. I'm not yet sure why the other one, that has much better performance, is off by 21.
                    String matchingMd5 = getMd5(i, part2);
                    Optional<String> quintTriple = getInternalTriple(matchingMd5);
                    if (quintTriple.isPresent() && containsMatchingQuintuple(matchingMd5, tripleStr)) {
                        validKeys.put(index, md5key);
                        break;
                    }
                }
            }
            generatedMd5s.remove(index);
            index++;
        }
    }

    public String getMd5(int index, boolean part2) {
        String md5key;
        if (!generatedMd5s.containsKey(index)) {
            md5key = md5Hexer.getHex(salt+index).toLowerCase();
            if (part2) {
                for (int i=0; i<2016; i++) {
                    md5key = md5Hexer.getHex(md5key).toLowerCase();
                }
            }
            generatedMd5s.put(index, md5key);
        } else {
            md5key = generatedMd5s.get(index);
        }
        return md5key;
    }

    public int getLastIndex() {
        return validKeys.lastKey();
    }


    public static Optional<String> getInternalTriple(String str) {
        Matcher match = TRIPLE_PATTERN.matcher(str);
        if (match.find()) {
            char c = match.group().charAt(0);
            return Optional.of("" + c + c + c);
        }
        return Optional.empty();
    }

    public static boolean containsMatchingQuintuple(String str, String tripleToMatch) {
        char c = tripleToMatch.charAt(0);
        String quintuple = tripleToMatch + c + c;
        return str.contains(quintuple);
    }
}
