package com.websysdev.adventofcode.day6;

import com.websysdev.adventofcode.shared.FileReader;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.*;

/**
 * Created by Mark on 08/12/2016.
 */
public class Application {

    public static void main(String[] args) throws IOException, URISyntaxException {
        if (args.length == 0) {
            System.out.println("Please provide a filename.");
            System.exit(0);
        }
        List<String> fileContents = FileReader.readFile(args[0]);
        System.out.println("Loaded " + fileContents.size() + " lines from the file.");
        String message = findMessage(fileContents, new MinCharFinder());
        System.out.println("Found message: " + message);
    }

    /**
     * Find the hidden message in the corrupted input, based upon regularity.
     *
     * @param input The list of Strings to check.
     * @param charFinder An implementation of CharFinder, to find the character of interest for the given column of counts.
     * @return The hidden message.
     */
    public static String findMessage(List<String> input, CharFinder charFinder) {
        long start = System.currentTimeMillis();
        char[][] charArray = toCharArray(input);
        // Almost do this in reverse - iterate over each column first, then move to the next row.
        Map<Character, Integer>[] counts = new HashMap[charArray[0].length];

        for (int x=0; x < charArray[0].length; x++) {
            counts[x] = new HashMap<>();
            for (int y=0; y < charArray.length; y++) {
                int count = 0;
                if (counts[x].containsKey(charArray[y][x])) {
                    count = counts[x].get(charArray[y][x]);
                }
                counts[x].put(charArray[y][x], ++count);
            }
        }

        StringBuilder sb = new StringBuilder();
        for (int col=0; col < counts.length; col++) {
            Character c = charFinder.find(counts[col]);
            sb.append(c);
        }
        long end = System.currentTimeMillis();
        System.out.println("Time taken: " + (end - start));
        return sb.toString();
    }

    /**
     * Converts a List of Strings into a two dimensional character array.
     * @param input A List of Strings.
     * @return A potentially uneven character array representing the list.
     */
    private static char[][] toCharArray(List<String> input) {
        char[][] charArray = new char[input.size()][];
        int i = 0;
        for (String line : input) {
            charArray[i++] = line.toCharArray();
        }
        return charArray;
    }

    /**
     * Finds a single character of interest from a map of character counts.
     */
    interface CharFinder {
        Character find(Map<Character,Integer> map);
    }

    /**
     * Finds a single character of interest from a map of character counts,
     * choosing the character that has appeared most often.
     */
    static class MaxCharFinder implements CharFinder {
        public Character find(Map<Character,Integer> map) {
            int biggestCount = 0;
            Character c = null;
            for (Map.Entry<Character, Integer> entry : map.entrySet()) {
                if (entry.getValue() > biggestCount) {
                    c = entry.getKey();
                    biggestCount = entry.getValue();
                }
            }
            return c;
        }
    }

    /**
     * Finds a single character of interest from a map of character counts,
     * choosing the character that has appeared least often.
     */
    static class MinCharFinder implements CharFinder {
        public Character find(Map<Character,Integer> map) {
            int smallestCount = Integer.MAX_VALUE;
            Character c = null;
            for (Map.Entry<Character, Integer> entry : map.entrySet()) {
                if (entry.getValue() < smallestCount) {
                    c = entry.getKey();
                    smallestCount = entry.getValue();
                }
            }
            return c;
        }
    }

}
