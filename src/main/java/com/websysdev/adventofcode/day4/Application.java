package com.websysdev.adventofcode.day4;

import com.websysdev.adventofcode.shared.FileReader;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.*;

/**
 * Created by Mark on 06/12/2016.
 */
public class Application {

    private static final int NUM_CHARS_IN_CHECKSUM = 5;
    static final String ALPHABET = "abcdefghijklmnopqrstuvwxyz";

    public static void main(String[] args) throws IOException, URISyntaxException {
        if (args.length == 0) {
            System.out.println("Please provide a filename.");
            System.exit(0);
        }
        List<String> fileContents = FileReader.readFile(args[0]);
        System.out.println("Loaded " + fileContents.size() + " lines from the file.");
        int totalRealRooms = 0;
        int sectorIdTotal = 0;
        for (String line : fileContents) {
            if (isRealRoom(line)) {
                totalRealRooms++;
                sectorIdTotal += getSectorId(line);
            }
        }
        System.out.println("Total Real Rooms: " + totalRealRooms);
        System.out.println("Sector ID Total: " + sectorIdTotal);
        System.out.println("");
        System.out.println("---- DECRYPTIONS ----");
        for (String line : fileContents) {
            System.out.println(decryptRoom(line) + "          " + line);
        }
    }

    public static String decryptRoom(String sequence) {
        String encryptedPart = getEncryptedPart(sequence);
        int sectorId = getSectorId(sequence);

        // Ok let's do this!
        StringBuilder decryptedString = new StringBuilder();
        for (char c : encryptedPart.toCharArray()) {
            if (c == '-') {
                decryptedString.append(" ");
                continue;
            }
            int pos = ALPHABET.indexOf(c);
            decryptedString.append(Application.ALPHABET.charAt((pos+sectorId) % 26)); // MATHS!
        }
        return decryptedString.toString();
    }

    public static boolean isRealRoom(String sequence) {
        String encryptedPart = getEncryptedPart(sequence);

        encryptedPart = encryptedPart.replace("-", "");
        Map<Character, Integer> characterCounts = new TreeMap<>();
        for (Character c : encryptedPart.toCharArray()) {
            int count = 0;
            if (characterCounts.containsKey(c)) {
                count = characterCounts.get(c);
            }
            characterCounts.put(c, ++count);
        }
        StringBuilder topFiveChars = new StringBuilder();
        int previousCount = 100;
        while (topFiveChars.length() < NUM_CHARS_IN_CHECKSUM) {
            int largestCount = 0;
            char charToAdd = 0;
            for (Map.Entry<Character, Integer> entry : characterCounts.entrySet()) {
                int entryCount = entry.getValue();
                if (entryCount <= previousCount && entryCount > largestCount) {
                    largestCount = entryCount;
                    charToAdd = entry.getKey();
                }
            }
            topFiveChars.append(charToAdd);
            characterCounts.remove(charToAdd);
            previousCount = largestCount;
        }
        String checksum = getChecksum(sequence);
        String calculatedChecksum = topFiveChars.toString();
        return checksum.equalsIgnoreCase(calculatedChecksum);
    }

    public static String getEncryptedPart(String sequence) {
        String fullSection = sequence.substring(0, sequence.indexOf('['));
        // Remove the sector id
        return fullSection.substring(0, fullSection.lastIndexOf('-'));
    }

    public static int getSectorId(String sequence) {
        String fullSection = sequence.substring(0, sequence.indexOf('['));
        String[] parts = fullSection.split("-");
        return Integer.parseInt(parts[parts.length-1]);
    }

    public static String getChecksum(String sequence) {
        String lastPart = sequence.substring(sequence.indexOf('['));
        return lastPart.substring(1, lastPart.indexOf(']'));
    }

}
