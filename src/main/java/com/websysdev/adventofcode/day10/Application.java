package com.websysdev.adventofcode.day10;

import com.websysdev.adventofcode.day10.bots.Bot;
import com.websysdev.adventofcode.day10.bots.BotInstruction;
import com.websysdev.adventofcode.shared.FileReader;
import javafx.util.Pair;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by Mark on 13/12/2016.
 */
public class Application {

    private Map<Integer, Bot> bots = new HashMap<>();
    private Map<Integer, Set<Integer>> outputs = new HashMap<>();

    private List<BotInstruction> botInstructions = new ArrayList<>();

    public static void main(String[] args) throws IOException, URISyntaxException {
        if (args.length == 0) {
            System.out.println("Please provide a filename.");
            System.exit(0);
        }
        List<String> fileContents = FileReader.readFile(args[0]);
        Application app = new Application(fileContents);
        app.runSimulation();
        Set<Integer> chipsToCompare = Stream.of(17, 61).collect(Collectors.toSet());
        Optional<Bot> bot = app.findBotResponsibleForChips(chipsToCompare);
        if (!bot.isPresent()) {
            System.out.println("No bot found!");
        } else {
            System.out.println("Bot " + bot.get().getId() + " is responsible for comparing " + chipsToCompare);
        }
        printPartTwo(app);
    }

    /* What do you get if you multiply together the values of one chip in each of outputs 0, 1, and 2? */
    public static void printPartTwo(Application app) {
        Set<Integer> output0 = app.getOutputs().get(0);
        Set<Integer> output1 = app.getOutputs().get(1);
        Set<Integer> output2 = app.getOutputs().get(2);

        int result = output0.iterator().next() * output1.iterator().next() * output2.iterator().next();

        System.out.println("Multiplied value: " + result);
    }



    public Application(List<String> input) {
        for (String line : input) {
            if (line.startsWith("value")) {
                processValueInput(line);
            } else {
                botInstructions.add(new BotInstruction(line));
            }
        }
    }

    private void processValueInput(String line) {
        // e.g. "value 5 goes to bot 2"
        String[] inputParts = line.split(" ");
        int id = Integer.parseInt(inputParts[5]);
        Bot bot;
        if (bots.containsKey(id)) {
            bot = bots.get(id);
        } else {
            bot = new Bot(id);
        }
        bot.receiveChip(Integer.parseInt(inputParts[1]));
        bots.put(bot.getId(), bot);
    }

    public void runSimulation() {
        int pos = 0;
        while (true) {
            if (botInstructions.isEmpty()) {
                break;
            }
            if (pos >= botInstructions.size()) {
                // start again from the beginning
                pos = 0;
            }
            BotInstruction instruction = botInstructions.get(pos);
            Bot bot = bots.get(instruction.getBotId());
            if (bot == null || bot.getChips().size() != 2) {
                // Bot isn't ready for processing. Move on.
                pos++;
                continue;
            }
            Iterator<Integer> it = bot.getChips().iterator();
            handleInstruction(instruction.getLowInstruction(), it.next());
            handleInstruction(instruction.getHighInstruction(), it.next());
            bot.processChips();
            botInstructions.remove(pos);
        }
    }

    private void handleInstruction(Pair<BotInstruction.Destination, Integer> instruction, int value) {
        switch (instruction.getKey()) {
            case BOT:
                Bot bot = new Bot(instruction.getValue());
                if (bots.containsKey(instruction.getValue())) {
                    bot = bots.get(instruction.getValue());
                }
                bot.receiveChip(value);
                bots.put(instruction.getValue(), bot);
                break;
            case OUTPUT:
                Set<Integer> values = new HashSet<>();
                if (outputs.containsKey(instruction.getValue())) {
                    values = outputs.get(instruction.getValue());
                }
                values.add(value);
                outputs.put(instruction.getValue(), values);

                break;
        }
    }

    public Map<Integer, Bot> getBots() {
        return bots;
    }

    public Map<Integer, Set<Integer>> getOutputs() {
        return outputs;
    }

    public Optional<Bot> findBotResponsibleForChips(Set<Integer> chipsToFind) {
        for (Bot bot : bots.values()) {
            if (bot.getProcessedChips().equals(chipsToFind)) {
                return Optional.of(bot);
            }
        }
        return Optional.empty();
    }
}
