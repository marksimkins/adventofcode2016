package com.websysdev.adventofcode.day10.bots;

import java.util.Set;
import java.util.TreeSet;

/**
 * Created by Mark on 13/12/2016.
 */
public class Bot {

    private int id;
    private Set<Integer> chips = new TreeSet<>();
    private Set<Integer> processedChips = new TreeSet<>();

    public Bot(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void receiveChip(int chip) {
        chips.add(chip);
    }

    public void processChips() {
        processedChips.addAll(chips);
        chips.clear();
    }

    public Set<Integer> getChips() {
        return chips;
    }

    public Set<Integer> getProcessedChips() {
        return processedChips;
    }
}
