package com.websysdev.adventofcode.day10.bots;

import javafx.util.Pair;

/**
 * Created by Mark on 13/12/2016.
 */
public class BotInstruction {

    public enum Destination {
        BOT,
        OUTPUT
    }

    private int botId;
    private Pair<Destination, Integer> lowInstruction;
    private Pair<Destination, Integer> highInstruction;

    public BotInstruction(String line) {
        String[] lineParts = line.split(" ");
        botId = Integer.parseInt(lineParts[1]);
        lowInstruction = new Pair<>(lineParts[5].equals("bot") ? Destination.BOT : Destination.OUTPUT, Integer.parseInt(lineParts[6]));
        highInstruction = new Pair<>(lineParts[10].equals("bot") ? Destination.BOT : Destination.OUTPUT, Integer.parseInt(lineParts[11]));
    }

    public int getBotId() {
        return botId;
    }

    public Pair<Destination, Integer> getLowInstruction() {
        return lowInstruction;
    }

    public Pair<Destination, Integer> getHighInstruction() {
        return highInstruction;
    }

}
