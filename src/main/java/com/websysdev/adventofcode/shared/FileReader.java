package com.websysdev.adventofcode.shared;

import com.websysdev.adventofcode.day3.Application;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

/**
 * Created by Mark on 06/12/2016.
 */
public class FileReader {

    public static List<String> readFile(String filename) throws IOException, URISyntaxException {
        URL url = Application.class.getClassLoader().getResource(filename);
        return Files.readAllLines(Paths.get(url.toURI()));
    }

}
