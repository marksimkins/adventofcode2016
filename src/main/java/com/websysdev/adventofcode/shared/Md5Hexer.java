package com.websysdev.adventofcode.shared;

import javax.xml.bind.annotation.adapters.HexBinaryAdapter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by Mark on 07/12/2016.
 */
public class Md5Hexer {

    private MessageDigest digest;
    private HexBinaryAdapter adapter = new HexBinaryAdapter();

    public Md5Hexer() {
        try {
            digest = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            System.out.println("Cannot run!");
            System.exit(1);
        }
    }

    public String getHex(String value) {
        return adapter.marshal(digest.digest(value.getBytes()));
    }

}
