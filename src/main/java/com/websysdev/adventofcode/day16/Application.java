package com.websysdev.adventofcode.day16;

import java.util.Arrays;

/**
 * This is using a very naive, simple and literal approach of using and modifying byte arrays.
 *
 * This can be done with int/long values and bitwise operators, which I'm sure would be far faster and less memory intensive...
 * but I admit I'm a bit rusty with those. I'll revisit this in the future.
 *
 * Created by Mark on 22/12/2016.
 */
public class Application {

    public static void main(String[] args) {
        String startInput = "00111101111101000";
        final int partOneDiskSize = 272;
        final int partTwoDiskSize = 35651584;
        long start = System.currentTimeMillis();
        byte[] grownData = fillDisk(stringToByteArray(startInput), partTwoDiskSize);
        String checksum = calcChecksum(byteArrayToString(grownData), partTwoDiskSize);
        System.out.println("Checksum: " + checksum);
        System.out.println("Time taken: " + (System.currentTimeMillis() - start));
    }

    public static byte[] reverse(byte[] byteArr) {
        byte[] reversedArr = new byte[byteArr.length];
        for (int i=0; i<byteArr.length; i++) {
            reversedArr[reversedArr.length-i-1] = byteArr[i];
        }
        return reversedArr;
    }

    public static byte[] swapBytes(byte[] byteArr) {
        // Consider using the ~ operator on an int - that flips the bits.
        byte[] swappedArr = new byte[byteArr.length];
        for (int i=0; i<byteArr.length; i++) {
            byte b = byteArr[i]; //awful!
            if (b == 0) {
                b = 1;
            } else {
                b = 0;
            }
            swappedArr[i] = b;
        }
        return swappedArr;
    }

    public static byte[] growData(byte[] byteArr) {
        byte[] newData = reverse(byteArr);
        newData = swapBytes(newData);
        byte[] grownData = new byte[byteArr.length + 1 + byteArr.length];
        System.arraycopy(byteArr, 0, grownData, 0, byteArr.length);
        System.arraycopy(newData, 0, grownData, byteArr.length+1, newData.length);
        return grownData;
    }

    public static byte[] fillDisk(byte[] bytes, int maxDiskSize) {
        while (bytes.length < maxDiskSize) {
            bytes = growData(bytes);
        }
        return bytes;
    }

    public static String calcChecksum(String baseString, int diskSize) {
        String sourceString = (baseString.length() > diskSize) ? baseString.substring(0, diskSize) : baseString;
        while (true) {
            StringBuilder checksum = new StringBuilder();

            int pos = 0;
            while (pos < sourceString.length()) {
                char first = sourceString.charAt(pos++);
                char second = sourceString.charAt(pos++);
                if (first == second) {
                    checksum.append(1);
                } else {
                    checksum.append(0);
                }
            }

            if (checksum.length() % 2 != 0) {
                // It's odd! Return.
                return checksum.toString();
            }
            // It's even. Use this as a base and try again.
            sourceString = checksum.toString();
        }
    }

    public static byte[] stringToByteArray(String str) {
        byte[] byteArr = new byte[str.length()];
        int pos = 0;
        for (char c : str.toCharArray()) {
            byteArr[pos++] = (byte) ((c == '1') ? 1 : 0);
        }
        return byteArr;
    }

    private static String byteArrayToString(byte[] byteArr) {
        String str = Arrays.toString(byteArr);
        return str.replace("[", "").replace("]", "").replace(",", "").replace(" ", "");
    }
}
