package com.websysdev.adventofcode.day11;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Mark on 13/12/2016.
 *
 * This is utter bullshit!
 */
public class Application {

    private static Item[][] floors;

    public static void main(String[] args) {
        /*
        The first floor contains a thulium generator, a thulium-compatible microchip, a plutonium generator, and a strontium generator.
        The second floor contains a plutonium-compatible microchip and a strontium-compatible microchip.
        The third floor contains a promethium generator, a promethium-compatible microchip, a ruthenium generator, and a ruthenium-compatible microchip.
        The fourth floor contains nothing relevant.

        F4
        F3  PRG RG  PRM RM
        F2  PLM  SM
        F1  TG  PLG  SG  TM

        Need to process the 'board', finding the next save move to make, with the aim to move all to the top. If on top, don't move again.
        Not too sure how to ensure minimum steps.
         */
        initFloors();
        System.out.println(Arrays.deepToString(floors));

        Timer timer = new Timer(3000, (ActionEvent arg0) -> System.out.println(Arrays.deepToString(floors)));
        timer.setRepeats(true); // Only execute once
        timer.start(); // Go go go!

        int steps = process();
        System.out.println("Steps: " + steps);
        System.out.println();
        System.out.println(Arrays.deepToString(floors));
    }

    private static void initFloors() {
        floors = new Item[][]{
                {new Generator(Element.THULIUM), new Generator(Element.PLUTONIUM), new Generator(Element.STRONTIUM), new Microchip(Element.THULIUM), null, null, null, null, null, null}, // Floor 1
                {new Microchip(Element.PLUTONIUM), new Microchip(Element.STRONTIUM), null, null, null, null, null, null, null, null}, // Floor 2
                {new Generator(Element.PROMETHIUM), new Generator(Element.RUTHENIUM), new Microchip(Element.PROMETHIUM), new Microchip(Element.RUTHENIUM), null, null, null, null, null, null}, // Floor 3
                {null, null, null, null, null, null, null, null, null, null}, // Floor 4
        };
    }

    private static int process() {
        int steps = 0;
        while (true) {
            loop : {
                if (floorSpaces(3) == 0) {
                    break;
                }
                List<Item> elevator = new ArrayList<>();
                int toFloor = 0;
                for (int i = 0; i < floors.length; i++) {
                    for (int j = 0; j < floors[i].length; j++) {
                        //for (Item eItem : elevator) {
                            if (floorIsSafe(i)) {
                                if (floorSpaces(i) > elevator.size()) {
                                    elevator.add(floors[i][j]);
                                    if (elevator.size() == 2) {
                                        steps++;
                                        moveItems(elevator, toFloor);
                                        break loop;
                                    }
                                }
                            }
                        //}
                        toFloor = i;
                    }
                }
                if (floorSpaces(toFloor) > elevator.size() && elevator.size() > 0) {
                    steps++;
                    moveItems(elevator, toFloor);
                }
            }
        }
        return steps;
    }

    private static int floorSpaces(int floor) {
        int spaces = 0;
        for (Item item : floors[floor]) {
            if (item == null) {
                spaces++;
            }
        }
        return spaces;
    }

    private static void moveItems(List<Item> elevator, int toFloor) {
        Iterator<Item> it = elevator.iterator();
        for (int j=0; j<floors[toFloor].length; j++) {
            while (it.hasNext()) {
                if (floors[toFloor][j] == null) {
                    floors[toFloor][j] = it.next();
                }
            }
        }
        checkFloors();
    }

    private static void checkFloors() {
        for (int i=0; i<floors.length; i++) {
            floorIsSafe(i);
        }
    }

    private static boolean floorIsSafe(int floor) {
        for (int j = 0; j < floors[floor].length; j++) {
            Item item = floors[floor][j];
            if (item == null) {
                continue;
            }
            for (int k = j; k < floors[floor].length; k++) {
                Item toCompare = floors[floor][k];
                if (toCompare == null) {
                    continue;
                }
                if (!item.isSafe(toCompare)) {
                    System.out.println(String.format("Found an unsafe floor! Floor %d, items %s and %s", floor, item, toCompare));
                    return false;
                }
            }
        }
        return true;
    }


    enum Element {
        THULIUM,
        PLUTONIUM,
        STRONTIUM,
        PROMETHIUM,
        RUTHENIUM
    }

    interface Item {
        boolean isSafe(Item item);
        Element getElement();
    }

    static class Generator implements Item {

        private Element element;

        Generator(Element element) {
            this.element = element;
        }

        public boolean isSafe(Item item) {
            if (item instanceof Generator) {
                return true;
            }
            return element.equals(item.getElement());
        }

        public Element getElement() {
            return element;
        }

        @Override
        public String toString() {
            return element + "_G";
        }
    }


    static class Microchip implements Item {

        private Element element;

        Microchip(Element element) {
            this.element = element;
        }

        public boolean isSafe(Item item) {
            if (item instanceof Microchip) {
                return true;
            }
            return element.equals(item.getElement());
        }

        public Element getElement() {
            return element;
        }

        @Override
        public String toString() {
            return element + "_M";
        }
    }

}
