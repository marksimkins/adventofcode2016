package com.websysdev.adventofcode.day1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Advent of Code, Day 1: http://adventofcode.com/2016/day/1
 *
 * Created by Mark on 02/12/2016.
 */
public class Application {

    private enum Compass {
        NORTH,
        EAST,
        SOUTH,
        WEST
    }

    public static void main(String[] args) {
        if (args.length == 0) {
            System.out.println("Please provide a CSV of directions.");
            System.exit(0);
        }
        int distance = getDistance(args[0]);
        System.out.println("Given these directions: ");
        System.out.println("    \"" + args[0] + "\"");
        System.out.println("You would have to walk " + distance + " blocks.");
    }

    /**
     * Calculate the distance you'd need to travel from your starting point, given a list of directions.
     * <br/>
     * I'm sure there's a neater more mathematical way to solve this, rather than my somewhat brute-force method!
     *
     * @param csv The basic raw CSV of directions
     * @return The distance from the starting point.
     */
    public static int getDistance(String csv) {
        List<DirectionInstruction> directions = toDirections(getInputs(csv));
        int yCoordinate = 0;
        int xCoordinate = 0;
        Compass previousDir = Compass.NORTH;
        for (DirectionInstruction instruction : directions) {
            if (previousDir == Compass.NORTH || previousDir == Compass.SOUTH) {
                if (previousDir == Compass.NORTH) {
                    if (instruction.direction == DirectionInstruction.Direction.LEFT) {
                        xCoordinate -= instruction.distance;
                        previousDir = Compass.WEST;
                    } else {
                        xCoordinate += instruction.distance;
                        previousDir = Compass.EAST;
                    }
                } else {
                    if (instruction.direction == DirectionInstruction.Direction.LEFT) {
                        xCoordinate += instruction.distance;
                        previousDir = Compass.EAST;
                    } else {
                        xCoordinate -= instruction.distance;
                        previousDir = Compass.WEST;
                    }
                }
            } else {
                if (previousDir == Compass.EAST) {
                    if (instruction.direction == DirectionInstruction.Direction.LEFT) {
                        yCoordinate += instruction.distance;
                        previousDir = Compass.NORTH;
                    } else {
                        yCoordinate -= instruction.distance;
                        previousDir = Compass.SOUTH;
                    }
                } else {
                    if (instruction.direction == DirectionInstruction.Direction.LEFT) {
                        yCoordinate -= instruction.distance;
                        previousDir = Compass.SOUTH;
                    } else {
                        yCoordinate += instruction.distance;
                        previousDir = Compass.NORTH;
                    }
                }
            }
        }
        return Math.abs(xCoordinate) + Math.abs(yCoordinate);
    }

    /** Create a sanitised List of inputs from the commandline. */
    public static List<String> getInputs(String csv) {
        if (csv == null) {
            return new ArrayList<>();
        }
        return Arrays.stream(csv.split(",")).map(String::trim).collect(Collectors.toList());
    }

    /** Convert the List of Strings into a List of DirectionInstructions. */
    public static List<DirectionInstruction> toDirections(List<String> rawInputs) {
        List<DirectionInstruction> directionList = new ArrayList<>();
        for (String input : rawInputs) {
            try {
                DirectionInstruction.Direction direction = DirectionInstruction.Direction.fromString(input.substring(0, 1));
                int distance = Integer.valueOf(input.substring(1));
                directionList.add(new DirectionInstruction(direction, distance));
            } catch (NumberFormatException nfe) {
                System.out.println("Cannot process direction: " + input);
            }
        }
        return directionList;
    }


    /** Class to wrap both the direction and the distance. */
    static class DirectionInstruction {
        public enum Direction {
            LEFT,
            RIGHT;

            public static Direction fromString(String string) {
                if (string != null) {
                    if (string.equalsIgnoreCase("L")) {
                        return LEFT;
                    } else if (string.equalsIgnoreCase("R")) {
                        return RIGHT;
                    }
                }
                return null;
            }
        }
        public Direction direction;
        public int distance;

        public DirectionInstruction(Direction direction, int distance) {
            this.direction = direction;
            this.distance = distance;
        }
    }

}
