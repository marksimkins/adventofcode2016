package com.websysdev.adventofcode.day15;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mark on 16/12/2016.
 */
public class Application {

    private List<Disk> disks = new ArrayList<>();

    public static void main(String[] args) {

        Application app = new Application();
        app.addDisk(17,1);
        app.addDisk(7,0);
        app.addDisk(19,2);
        app.addDisk(5,0);
        app.addDisk(3,0);
        app.addDisk(13,5);

        // Start Part 2
        app.addDisk(11, 0);
        // End Part 2

        System.out.println("Running simulation...");
        long start = System.currentTimeMillis();
        int firstTimeToRelease = app.calculateTimeToRelease();
        long end = System.currentTimeMillis();

        System.out.println("Given the disks, you should release the capsule after " + firstTimeToRelease + " seconds.");
        System.out.println("That took " + (end-start) + " milliseconds to calculate.");
    }

    public void addDisk(int numDiskPositions, int startPosition) {
        disks.add(new Disk(numDiskPositions, startPosition));
    }

    /*
    I'm proud of this! Part 1 took only 15 milliseconds to calculate, and Part 2 only 29.
     */
    public int calculateTimeToRelease() {
        int currentTime = 0;
        while (true) {
            // Just give up if you're here!
            if (currentTime == Integer.MAX_VALUE) {
                return -1;
            }
            boolean hasClearPath = true;
            for (int i = 0; i < disks.size(); i++) {
                Disk disk = disks.get(i);
                int result = getSimulatedPosition(disk, currentTime+i+1);
                if (result != 0) {
                    hasClearPath = false;
                    break;
                }
            }
            if (hasClearPath) {
                return currentTime;
            }
            currentTime++;
        }
    }

    private int getSimulatedPosition(Disk disk, int time) {
        return (disk.getPosition() + time) % disk.getNumDiskPositions();
    }

    public static class Disk {
        private int numDiskPositions;
        private int currentPosition;

        public Disk(int numDiskPositions, int currentPosition) {
            this.numDiskPositions = numDiskPositions;
            this.currentPosition = currentPosition;
        }

        int getNumDiskPositions() {
            return numDiskPositions;
        }

        public int getPosition() {
            return currentPosition;
        }
    }
}
