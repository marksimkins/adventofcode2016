package com.websysdev.adventofcode.day5;

import com.websysdev.adventofcode.shared.Md5Hexer;

/**
 * Created by Mark on 07/12/2016.
 */
public class Application {

    private static final String WANTED_PREFIX = "00000";

    public static void main(String[] args) {
        String doorId = "ugkcyxxp";
        /*String password = retrievePassword(doorId);
        System.out.println(String.format("With your doorId of '%s', I calculate the password '%s'.", doorId, password));*/
        String password = retrievePasswordPart2(doorId);
        System.out.println(String.format("With your doorId of '%s', I calculate the second password '%s'.", doorId, password));
    }

    public static String retrievePassword(String doorId) {
        long startTime = System.currentTimeMillis();
        StringBuilder password = new StringBuilder();

        Md5Hexer hexer = new Md5Hexer();
        int i = 0;
        while(password.length() < 8) {
            String hex = hexer.getHex(doorId + i);
            if (hex.startsWith(WANTED_PREFIX)) {
                password.append(hex.charAt(5));
            }
            i++;
        }

        long endTime = System.currentTimeMillis();
        System.out.println(String.format("Method took %d milliseconds to run.", endTime-startTime));
        return password.toString().toLowerCase();
    }

    public static String retrievePasswordPart2(String doorId) {
        long startTime = System.currentTimeMillis();
        char[] password = new char[8];

        Md5Hexer hexer = new Md5Hexer();
        int i = 0;
        int setCharacters = 0;
        while(setCharacters < 8) {
            String hex = hexer.getHex(doorId + i);
            if (hex.startsWith(WANTED_PREFIX)) {
                // Now, look at the 6th character. If it's 0-7, use it, if not, continue.
                char posc = hex.charAt(5);
                if (Character.isDigit(posc)) {
                    int pos = Character.getNumericValue(posc);
                    if (pos <= 7 && password[pos] == 0) {
                        char val = hex.charAt(6);
                        password[pos] = val;
                        setCharacters++;
                    }
                }
            }
            i++;
        }

        long endTime = System.currentTimeMillis();
        System.out.println(String.format("Method took %d milliseconds to run.", endTime-startTime));
        return String.valueOf(password).toLowerCase();
    }

}
