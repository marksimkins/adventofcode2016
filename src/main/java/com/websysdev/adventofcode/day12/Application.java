package com.websysdev.adventofcode.day12;

import com.websysdev.adventofcode.shared.FileReader;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.*;

/**
 * Created by Mark on 14/12/2016.
 */
public class Application {

    private Map<String, Integer> registers = new HashMap<>();

    private final String[] instructions;

    public static void main(String[] args) throws IOException, URISyntaxException {
        if (args.length == 0) {
            System.out.println("Please provide a filename.");
            System.exit(0);
        }
        List<String> fileContents = FileReader.readFile(args[0]);
        Application app = new Application(fileContents);
        app.process();
        Integer register = app.getRegister("a");
        System.out.println("Value left in register a is: " + register);
    }

    public Application(List<String> instructions) {
        this.instructions = instructions.toArray(new String[]{});
        // Part 2 initialisation, comment this line out for Part 1
        registers.put("c", 1);
    }

    /*
    cpy x y     copies x (either an integer or the value of a register) into register y.
    inc x       increases the value of register x by one.
    dec x       decreases the value of register x by one.
    jnz x y     jumps to an instruction y away (positive means forward; negative means backward), but only if x is not zero.
     */
    public void process() {
        int pos = 0;
        while (true) {
            if (pos >= instructions.length) {
                return;
            }
            String[] parts = instructions[pos].split(" ");
            switch (parts[0]) {
                case "cpy": {
                    Integer value = getIntOrRegValue(parts[1]);
                    String destRegName = parts[2];
                    Integer destReg = registers.get(destRegName);
                    registers.put(destRegName, copy(value, destReg));
;                   break;
                }
                case "inc": {
                    String destRegName = parts[1];
                    Integer destReg = registers.get(destRegName);
                    registers.put(destRegName, increment(destReg));
                    break;
                }
                case "dec": {
                    String destRegName = parts[1];
                    Integer destReg = registers.get(destRegName);
                    registers.put(destRegName, decrement(destReg));
                    break;
                }
                case "jnz": {
                    Integer source = getIntOrRegValue(parts[1]);
                    if (source != null) {
                        Integer jump = Integer.parseInt(parts[2]);
                        if (source != 0) {
                            pos += jump;
                            // This continue is important!
                            continue;
                        }
                    }
                    break;
                }
            }
            pos++;
        }
    }

    private Integer getIntOrRegValue(String strVal) {
        if (isInt(strVal)) {
            return Integer.parseInt(strVal);
        }
        Integer sourceReg = registers.get(strVal);
        if (sourceReg == null) {
            sourceReg = new Integer(0);
        }
        return sourceReg;
    }

    private boolean isInt(String value) {
        // Hate this. But don't want to import another library just for one method.
        try {
            Integer.parseInt(value);
            return true;
        } catch (NumberFormatException nfe) {
            return false;
        }
    }

    // Yes, these methods are a little redundant, but it's better encapsulation for testing and DRY.

    public Integer copy(Integer value, Integer destRegister) {
        destRegister = value;
        return destRegister;
    }

    public Integer increment(Integer reg) {
        return ++reg;
    }

    public Integer decrement(Integer reg) {
        return --reg;
    }

    public Integer getRegister(String registerName) {
        return registers.get(registerName);
    }

}
