package com.websysdev.adventofcode.day2;

/**
 * Advent of Code, Day 2: http://adventofcode.com/2016/day/2
 *
 * Created by Mark on 02/12/2016.
 */
public class Application {

    private static String[][] keypad = {
            { "1", "2", "3" },
            { "4", "5", "6" },
            { "7", "8", "9" },
    };

    private static String[][] keypad2 = {
            { "0",  "0",  "1", "0", "0" },
            { "0",  "2",  "3", "4", "0" },
            { "5",  "6",  "7", "8", "9" },
            { "0",  "A",  "B", "C", "0" },
            { "0",  "0",  "D", "0", "0" }
    };

    public static void main(String[] args) {
        String[] puzzleInputs = new String[] {
                "LRRLLLRDRURUDLRDDURULRULLDLRRLRLDULUDDDDLLRRLDUUDULDRURRLDULRRULDLRDUDLRLLLULDUURRRRURURULURRULRURDLULURDRDURDRLRRUUDRULLLLLDRULDDLLRDLURRLDUURDLRLUDLDUDLURLRLDRLUDUULRRRUUULLRDURUDRUDRDRLLDLDDDLDLRRULDUUDULRUDDRLLURDDRLDDUDLLLLULRDDUDDUUULRULUULRLLDULUDLLLLURRLDLUDLDDLDRLRRDRDUDDDLLLLLRRLLRLUDLULLDLDDRRUDDRLRDDURRDULLLURLRDLRRLRDLDURLDDULLLDRRURDULUDUDLLLDDDLLRLDDDLLRRLLURUULULDDDUDULUUURRUUDLDULULDRDDLURURDLDLULDUDUDDDDD",
                "RUURUDRDUULRDDLRLLLULLDDUDRDURDLRUULLLLUDUDRRUDUULRRUUDDURDDDLLLLRRUURULULLUDDLRDUDULRURRDRDLDLDUULUULUDDLUDRLULRUDRDDDLRRUUDRRLULUULDULDDLRLURDRLURRRRULDDRLDLLLRULLDURRLUDULDRDUDRLRLULRURDDRLUDLRURDDRDULUDLDLLLDRLRUDLLLLLDUDRDUURUDDUDLDLDUDLLDLRRDLULLURLDDUDDRDUDLDDUULDRLURRDLDLLUUDLDLURRLDRDDLLDLRLULUDRDLLLDRLRLLLDRUULUDLLURDLLUURUDURDDRDRDDUDDRRLLUULRRDRULRURRULLDDDUDULDDRULRLDURLUDULDLDDDLRULLULULUDLDDRDLRDRDLDULRRLRLRLLLLLDDDRDDULRDULRRLDLUDDDDLUDRLLDLURDLRDLDRDRDURRDUDULLLDLUDLDRLRRDDDRRLRLLULDRLRLLLLDUUURDLLULLUDDRLULRDLDLDURRRUURDUDRDLLLLLLDDDURLDULDRLLDUDRULRRDLDUDRLLUUUDULURRUR",
                "URRRLRLLDDDRRLDLDLUDRDRDLDUDDDLDRRDRLDULRRDRRDUDRRUUDUUUDLLUURLRDRRURRRRUDRLLLLRRDULRDDRUDLRLUDURRLRLDDRRLUULURLURURUDRULDUUDLULUURRRDDLRDLUDRDLDDDLRUDURRLLRDDRDRLRLLRLRUUDRRLDLUDRURUULDUURDRUULDLLDRDLRDUUDLRLRRLUDRRUULRDDRDLDDULRRRURLRDDRLLLRDRLURDLDRUULDRRRLURURUUUULULRURULRLDDDDLULRLRULDUDDULRUULRRRRRLRLRUDDURLDRRDDULLUULLDLUDDDUURLRRLDULUUDDULDDUULLLRUDLLLRDDDLUUURLDUDRLLLDRRLDDLUDLLDLRRRLDDRUULULUURDDLUR",
                "UULDRLUULURDRLDULURLUDULDRRDULULUDLLDURRRURDRLRLLRLDDLURRDLUUDLULRDULDRDLULULULDDLURULLULUDDRRULULULRDULRUURRRUDLRLURDRURDRRUDLDDUURDUUDLULDUDDLUUURURLRRDLULURDURRRURURDUURDRRURRDDULRULRRDRRDRUUUUULRLUUUDUUULLRRDRDULRDDULDRRULRLDLLULUUULUUDRDUUUDLLULDDRRDULUURRDUULLUUDRLLDUDLLLURURLUDDLRURRDRLDDURLDLLUURLDUURULLLRURURLULLLUURUUULLDLRDLUDDRRDDUUDLRURDDDRURUURURRRDLUDRLUULDUDLRUUDRLDRRDLDLDLRUDDDDRRDLDDDLLDLULLRUDDUDDDLDDUURLDUDLRDRURULDULULUDRRDLLRURDULDDRRDLUURUUULULRURDUUDLULLURUDDRLDDUDURRDURRUURLDLLDDUUDLLUURDRULLRRUUURRLLDRRDLURRURDULDDDDRDD",
                "LLRUDRUUDUDLRDRDRRLRDRRUDRDURURRLDDDDLRDURDLRRUDRLLRDDUULRULURRRLRULDUURLRURLRLDUDLLDULULDUUURLRURUDDDDRDDLLURDLDRRUDRLDULLRULULLRURRLLURDLLLRRRRDRULRUDUDUDULUURUUURDDLDRDRUUURLDRULDUDULRLRLULLDURRRRURRRDRULULUDLULDDRLRRULLDURUDDUULRUUURDRRLULRRDLDUDURUUUUUURRUUULURDUUDLLUURDLULUDDLUUULLDURLDRRDDLRRRDRLLDRRLUDRLLLDRUULDUDRDDRDRRRLUDUDRRRLDRLRURDLRULRDUUDRRLLRLUUUUURRURLURDRRUURDRRLULUDULRLLURDLLULDDDLRDULLLUDRLURDDLRURLLRDRDULULDDRDDLDDRUUURDUUUDURRLRDUDLRRLRRRDUULDRDUDRLDLRULDL"
        };
        System.out.println("Given the input sequence, the keycode is: " + getKeyCode(puzzleInputs));
        System.out.println("Given the input sequence, the second keycode is: " + getKeyCode2(puzzleInputs));
    }

    public static String getKeyCode(String[] inputs) {
        return getKeyCode(inputs, keypad, 1, 1);
    }

    public static String getKeyCode2(String[] inputs) {
        return getKeyCode(inputs, keypad2, 0, 2);
    }


    /**
     * "ULL",
     "RRDDD",
     "LURDL",
     "UUUUD"
     */

    /**
     * Given an array of 'direction' sequences for each number, calculate which number on the keypad should be pressed.
     *
     * @param inputs The array of direction sequences for each number.
     * @param keyPad The keypad layout to use.
     * @param startX The starting point in the X coordinate.
     * @param startY The starting point in the Y coordinate.
     * @return The resulting keypad code.
     */
    private static String getKeyCode(String[] inputs, String[][] keyPad, int startX, int startY) {
        StringBuilder keyCode = new StringBuilder();
        int xPos = startX;
        int yPos = startY;
        for (String digitInputs : inputs) {
            for (char c : digitInputs.toUpperCase().toCharArray()) {
                switch (c) {
                    case 'U':
                        if (yPos > 0 && !keyPad[yPos-1][xPos].equals("0")) {
                            yPos--;
                        }
                        break;
                    case 'D':
                        if (yPos < (keyPad.length - 1) && !keyPad[yPos+1][xPos].equals("0")) {
                            yPos++;
                        }
                        break;
                    case 'L':
                        if (xPos > 0 && !keyPad[yPos][xPos-1].equals("0")) {
                            xPos--;
                        }
                        break;
                    case 'R':
                        if (xPos < (keyPad[yPos].length - 1) && !keyPad[yPos][xPos+1].equals("0")) {
                            xPos++;
                        }
                        break;
                }
            }
            keyCode.append(keyPad[yPos][xPos]);
        }
        return keyCode.toString();
    }
}
