package com.websysdev.adventofcode.day3;

import com.websysdev.adventofcode.shared.FileReader;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Mark on 05/12/2016.
 */
public class Application {

    public static void main(String[] args) throws IOException, URISyntaxException {
        if (args.length == 0) {
            System.out.println("Please provide a filename.");
            System.exit(0);
        }
        List<String> fileContents = FileReader.readFile(args[0]);
        System.out.println("Loaded " + fileContents.size() + " lines from the file.");

        int numTriangles = getTrianglesByRow(fileContents);
        System.out.println("Given the input by rows, we have " + numTriangles + " triangles.");

        numTriangles = getTrianglesByColumn(fileContents);
        System.out.println("Given the input by columns, we have " + numTriangles + " triangles.");
    }

    public static int getTrianglesByRow(List<String> fileContents) {
        int numTriangles = 0;
        for (String line : fileContents) {
            List<String> dimensions = toDimensions(line);
            if (isTriangle(dimensions)) {
                numTriangles++;
            }
        }
        return numTriangles;
    }

    public static int getTrianglesByColumn(List<String> fileContents) {
        int numTriangles = 0;
        List<List<String>> sublist = new ArrayList<>();
        for (String line : fileContents) {
            if (sublist.size() < 3) {
                sublist.add(toDimensions(line));
            }
            if (sublist.size() < 3) {
                continue;
            }
            for (int i=0; i<3; i++) {
                List<String> dimensions = Arrays.asList(sublist.get(0).get(i), sublist.get(1).get(i), sublist.get(2).get(i));
                if (isTriangle(dimensions)) {
                    numTriangles++;
                }
            }
            sublist.clear();
        }
        return numTriangles;
    }

    public static List<String> toDimensions(String line) {
        if (line == null) {
            return new ArrayList<>();
        }
        line = line.trim();
        return Arrays.stream(line.split(" ")).map(String::trim).filter(s -> !s.isEmpty()).collect(Collectors.toList());
    }

    public static boolean isTriangle(List<String> dimensions) {
        /*
        Given some dimensions:
        5  10  25

        Need to add two together in all permutations, and see if they are all *greater* than the remaining number.
         */
        int a = Integer.parseInt(dimensions.get(0));
        int b = Integer.parseInt(dimensions.get(1));
        int c = Integer.parseInt(dimensions.get(2));

        return ( ((a + b) > c) && ((a + c) > b) && ((b + c) > a) );
    }

}
