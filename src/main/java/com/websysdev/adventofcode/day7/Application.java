package com.websysdev.adventofcode.day7;

import com.websysdev.adventofcode.shared.FileReader;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * Created by Mark on 09/12/2016.
 */
public class Application {

    public static void main(String[] args) throws IOException, URISyntaxException {
        if (args.length == 0) {
            System.out.println("Please provide a filename.");
            System.exit(0);
        }
        List<String> fileContents = FileReader.readFile(args[0]);
        System.out.println("Loaded " + fileContents.size() + " lines from the file.");
        int tlsCounter = 0;
        for (String line : fileContents) {
            if (supportsTls(line)) {
                tlsCounter++;
            }
        }
        System.out.println("IPs that support TLS: " + tlsCounter);
        int sslCounter = 0;
        for (String line : fileContents) {
            if (supportsSsl(line)) {
                sslCounter++;
            }
        }
        System.out.println("IPs that support SSL: " + sslCounter);
    }

    /*
    An IP supports TLS if it has an Autonomous Bridge Bypass Annotation, or ABBA. An ABBA is any four-character sequence
    which consists of a pair of two different characters followed by the reverse of that pair, such as xyyx or abba.
    However, the IP also must not have an ABBA within any hypernet sequences, which are contained by square brackets.
     */
    public static boolean supportsTls(String ipv7) {
        boolean inHypernetSequence = false;
        List<Character> sequence = new LinkedList<>();
        boolean hasSuitableAbba = false;
        for (Character c : ipv7.toCharArray()) {
            if (inHypernetSequence){
                if (c == ']') {
                    sequence.clear();
                    inHypernetSequence = false;
                    continue;
                }
            }
            if (c == '[') {
                // reset sequence counter
                sequence.clear();
                inHypernetSequence = true;
                continue;
            }
            sequence.add(c);
            if (sequence.size() == 4) {
                // check for ABBA sequence
                // Check that the first 2 are different, and then that they pair up
                Character c0 = sequence.get(0);
                Character c1 = sequence.get(1);
                Character c2 = sequence.get(2);
                Character c3 = sequence.get(3);

                if (!c0.equals(c1) && c0.equals(c3) && c1.equals(c2)) {
                    if (inHypernetSequence) {
                        return false;
                    }
                    hasSuitableAbba = true;
                }
                sequence.remove(0);
            }
        }
        return hasSuitableAbba;
    }

    /*
     * An IP supports SSL if it has an Area-Broadcast Accessor, or ABA, anywhere in the supernet sequences
     * (outside any square bracketed sections), and a corresponding Byte Allocation Block, or BAB, anywhere in the
     * hypernet sequences. An ABA is any three-character sequence which consists of the same character twice with a
     * different character between them, such as xyx or aba. A corresponding BAB is the same characters but in
     * reversed positions: yxy and bab, respectively.
     *
     * This is pretty simplistic, and could easily be optimised - however, it doesn't currently need to be.
     * I think this is quite readable and easy to see what's going on.
     */
    public static boolean supportsSsl(String ipv7) {
        boolean inHypernetSequence = false;
        List<Character> sequence = new LinkedList<>();
        Set<String> supernetAba = new HashSet<>();
        Set<String> hypernetAba = new HashSet<>();
        for (Character c : ipv7.toCharArray()) {
            if (inHypernetSequence){
                if (c == ']') {
                    sequence.clear();
                    inHypernetSequence = false;
                    continue;
                }
            }
            if (c == '[') {
                // reset sequence counter
                sequence.clear();
                inHypernetSequence = true;
                continue;
            }
            sequence.add(c);
            if (sequence.size() == 3) {
                // check for ABA sequence (or BAB, same difference at this point)
                // Check that the first 2 are different, and then that they pair up
                Character c0 = sequence.get(0);
                Character c1 = sequence.get(1);
                Character c2 = sequence.get(2);

                if (!c0.equals(c1) && c0.equals(c2)) {
                    if (inHypernetSequence) {
                        hypernetAba.add("" + c0 + c1 + c2);
                    } else {
                        supernetAba.add("" + c0 + c1 + c2);
                    }
                }
                sequence.remove(0);
            }
        }
        if (supernetAba.isEmpty() || hypernetAba.isEmpty()) {
            return false;
        }
        for (String sAba : supernetAba) {
            for (String hAba : hypernetAba) {
                if (sAba.charAt(0) == hAba.charAt(1) && sAba.charAt(1) == hAba.charAt(0)) {
                    return true;
                }
            }
        }
        return false;
    }
}
