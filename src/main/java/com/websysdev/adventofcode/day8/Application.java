package com.websysdev.adventofcode.day8;

import com.websysdev.adventofcode.shared.FileReader;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

/**
 * Unlike the others, this will require an instance of the class to run.
 * Created by Mark on 12/12/2016.
 */
public class Application {

    private static final int SCREEN_HEIGHT = 6;
    private static final int SCREEN_WIDTH = 50;

    private boolean[][] screenPixels;

    public static void main(String[] args) throws IOException, URISyntaxException {
        if (args.length == 0) {
            System.out.println("Please provide a filename.");
            System.exit(0);
        }
        List<String> fileContents = FileReader.readFile(args[0]);
        System.out.println("Loaded " + fileContents.size() + " lines from the file.");

        Application app = new Application();
        for (String line : fileContents) {
            app.processLine(line);
        }
        System.out.println(); // just a spacer
        app.printScreen();
        System.out.println(); // just a spacer
        System.out.println("There are " + app.countOnPixels() + " pixels turned on.");
    }

    public Application() {
        screenPixels = new boolean[SCREEN_HEIGHT][SCREEN_WIDTH];
    }

    /*
    rect AxB                turns on all of the pixels in a rectangle at the top-left of the screen which is A wide and B tall.
    rotate row y=A by B     shifts all of the pixels in row A (0 is the top row) right by B pixels. Pixels that would fall off the right end appear at the left end of the row.
    rotate column x=A by B  shifts all of the pixels in column A (0 is the left column) down by B pixels. Pixels that would fall off the bottom appear at the top of the column.
     */
    public void processLine(String line) {
        // I'd really like to find a better way to do this. This feels... far too manual.
        if (line.startsWith("rect")) {
            String[] halves = line.split(" ");
            String[] values = halves[1].split("x");
            setRectangle(Integer.parseInt(values[0]), Integer.parseInt(values[1]));
        } else {
            String[] halves = line.split("=");
            String[] values = halves[1].split(" by ");
            if (halves[0].contains("row")) {
                rotateRow(Integer.parseInt(values[0]), Integer.parseInt(values[1]));
            } else {
                rotateColumn(Integer.parseInt(values[0]), Integer.parseInt(values[1]));
            }
        }
    }

    public void setRectangle(int width, int height) {
        if (width > screenPixels[0].length) {
            width = screenPixels[0].length;
        }
        if (height > screenPixels.length) {
            height = screenPixels.length;
        }
        for (int w=0; w<width; w++) {
            for (int h=0; h<height; h++) {
                screenPixels[h][w] = true;
            }
        }
    }

    public void rotateRow(int row, int rotateBy) {
        if (row < screenPixels.length) {
            rotateArray(screenPixels[row], rotateBy);
        }
    }

    public void rotateColumn(int column, int rotateBy) {
        if (column < screenPixels[0].length) {
            boolean[] temp = new boolean[SCREEN_HEIGHT];
            for (int i=0; i<temp.length; i++) {
                temp[i] = screenPixels[i][column];
            }
            rotateArray(temp, rotateBy);
            for (int i=0; i<temp.length; i++) {
                screenPixels[i][column] = temp[i];
            }
        }
    }

    public void printScreen() {
        for (int i=0; i<screenPixels.length; i++) {
            StringBuilder sb = new StringBuilder();
            for (int j=0; j<screenPixels[i].length; j++) {
                sb.append(screenPixels[i][j]? "*" : " ");
            }
            System.out.println(sb.toString());
        }
    }

    public int countOnPixels() {
        int counter = 0;
        for (int i=0; i<screenPixels.length; i++) {
            for (int j=0; j<screenPixels[i].length; j++) {
                if (screenPixels[i][j]) {
                    counter++;
                }
            }
        }
        return counter;
    }

    // Shamefully grabbed off the internet, tweaked and commented by myself.
    private void rotateArray(boolean[] arr, int rotateBy) {
        if (rotateBy > arr.length){
            rotateBy = rotateBy % arr.length;
        }

        //length of first part
        int a = arr.length - rotateBy;

        // Example array: abcdefg
        // Example rotation: 2

        // This is the bit that ends up just being pushed to the right.
        // Example: edcbafg
        reverseArray(arr, 0, a-1);
        // Now for the bit that will be pushed to the start.
        // Example: edcbagf
        reverseArray(arr, a, arr.length-1);
        // Finally, reverse the whole array, pushing the bit that was at the end to the beginning.
        // Example: fgabcde
        reverseArray(arr, 0, arr.length-1);
    }

    // Shamefully grabbed off the internet.
    private void reverseArray(boolean[] arr, int left, int right){
        while(left < right){
            boolean temp = arr[left];
            arr[left] = arr[right];
            arr[right] = temp;
            left++;
            right--;
        }
    }

}
