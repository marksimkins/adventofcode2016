This is my attempt at the Advent of Code 2016, a kind of geeky advent calendar of small programming puzzles.

http://adventofcode.com/

I'll implement each day (if I'm not too busy) in it's own package within this same project.

**Puzzles Completed**
* Day 1
* Day 2 (1 + 2)
* Day 3 (1 + 2)
* Day 4 (1 + 2)
* Day 5 (1 + 2)
* Day 6 (1 + 2)
* Day 7 (1 + 2)
* Day 8 (1 + 2)
* Day 9
* Day 10 (1 + 2)
* Day 12 (1 + 2)
* Day 14 (1 + 2)
* Day 15 (1 + 2)
* Day 16 (1 + 2)